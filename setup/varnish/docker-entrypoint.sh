#!/usr/bin/env bash
set -e

export DOLLAR='$'
export BACKEND_SRV=${BACKEND_SRV:-missing_backend_srv}
export BACKEND_STATUS=${BACKEND_STATUS:-/status}
export BACKEND_TTL=${BACKEND_TTL:-10s}
export VARNISH_SIZE=${VARNISH_SIZE:-1g}
export CACHE_STORAGE_BACKEND=${CACHE_STORAGE_BACKEND:-file,/var/cache/varnish}
export CACHE_STORAGE=${CACHE_STORAGE:-$CACHE_STORAGE_BACKEND,$VARNISH_SIZE}
export TIMEOUT_IDLE=${TIMEOUT_IDLE:-65}
export MAX_RETRIES=${MAX_RETRIES:-4}

for TEMPLATE in /etc/varnish/*.template; do
    envsubst < $TEMPLATE > ${TEMPLATE/.template/}
done

# this will check if the first argument is a flag
# but only works if all arguments require a hyphenated flag
# -v; -SL; -f arg; etc will work, but not arg1 arg2
if [ "$#" -eq 0 ] || [ "${1#-}" != "$1" ]; then
    set -- varnishd -F -f /etc/varnish/default.vcl -a http=:80,HTTP -s $CACHE_STORAGE \
        -p timeout_idle=$TIMEOUT_IDLE \
        -p max_retries=$MAX_RETRIES \
        "$@"
fi

exec "$@"

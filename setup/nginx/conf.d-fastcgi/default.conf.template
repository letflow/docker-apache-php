server {
    listen 80;
    index index.php index.html;
    root ${FASTCGI_DOCROOT};

    # your host or vpc cidr_block or the docker gateway address ip:
    set_real_ip_from ${NGINX_TRUSTED};
    real_ip_header X-Forwarded-For;

    set ${DOLLAR}logme ${NGINX_ACCESS_LOG};
    if ( ${DOLLAR}uri ~ ${NGINX_ACCESS_LOG_IGNORE} ) {
        set ${DOLLAR}logme 0;
    }    
    access_log  /var/log/output.log main buffer=8k flush=1m if=${DOLLAR}logme;

    location / {
        try_files ${DOLLAR}uri /index.php?${DOLLAR}args;
    }

    location ~ \.php${DOLLAR} {
        fastcgi_split_path_info ^(.+\.php)(/.+)${DOLLAR};
        fastcgi_pass ${FASTCGI_HOST}:${FASTCGI_PORT};
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_read_timeout 600;
        fastcgi_param SCRIPT_FILENAME ${DOLLAR}document_root${DOLLAR}fastcgi_script_name;
        fastcgi_param PATH_INFO ${DOLLAR}fastcgi_path_info;
        fastcgi_param REMOTE_ADDR ${DOLLAR}http_x_forwarded_for;

        fastcgi_cache my_cache;
        add_header X-Cache ${DOLLAR}upstream_cache_status;
    }
}

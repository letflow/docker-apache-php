#!/usr/bin/env sh
export DOLLAR='$'
export FASTCGI_DOCROOT=${FASTCGI_DOCROOT:-/var/www/html/public}
export FASTCGI_HOST=${FASTCGI_HOST:-app}
export FASTCGI_PORT=${FASTCGI_PORT:-9000}
export NGINX_CACHE_MAX_SIZE=${NGINX_CACHE_MAX_SIZE:-1g}
export NGINX_ACCESS_LOG=${NGINX_ACCESS_LOG:-0}
export NGINX_ACCESS_LOG_IGNORE=${NGINX_ACCESS_LOG_IGNORE:-"(?!)"} # defaults to unmatchable string to always log
export NGINX_TRUSTED=${NGINX_TRUSTED:-"10.0.0.0/24"}
export NGINX_MAX_UPLOAD=${NGINX_MAX_UPLOAD:-"6M"}
export PROXY_DOCROOT=${PROXY_DOCROOT:-/var/www/html/public}
export PROXY_ORIGIN_HOST=${PROXY_ORIGIN_HOST:-app}
export PROXY_ORIGIN_PORT=${PROXY_ORIGIN_PORT:-9000}
export NGINX_USE_PHP=${NGINX_USE_PHP:-false}

envsubst < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf

if [[ ! -z $NGINX_CUSTOM_CONF ]]; then
    echo "Copying custom conf from $NGINX_CUSTOM_CONF/*.template to /etc/nginx/conf.d/"
    cp $NGINX_CUSTOM_CONF/*.template /etc/nginx/conf.d/ 2>/dev/null
fi

for TEMPLATE in /etc/nginx/conf.d/*.template; do
    envsubst < $TEMPLATE > ${TEMPLATE/.template/}
done

cp /etc/resolv.conf /etc/resolv.dnsmasq
dnsmasq -r /etc/resolv.dnsmasq

exec nginx -g "daemon off;"
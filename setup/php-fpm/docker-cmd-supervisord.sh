#!/bin/sh
set -e

export PHP_ENABLED=${PHP_ENABLED:-true}

# start supervisor daemon
exec supervisord -c /etc/supervisord.conf
#!/bin/sh

URL=$1
FILE=${URL%%\?*}
PHP_ROOT=/var/www/html/public
env -i QUERY_STRING=${URL#*\?} SCRIPT_NAME=${FILE} SCRIPT_FILENAME=${PHP_ROOT}${FILE} REQUEST_METHOD=GET cgi-fcgi -bind -connect 127.0.0.1:9000 | awk 'BEGIN{RS="\r\n\r\n"} NR==2'

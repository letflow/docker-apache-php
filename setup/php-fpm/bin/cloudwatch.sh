#!/bin/sh

SERVER_STATUS_URL="http://localhost/php-fpm-status"
SERVER_STATUS_HOSTNAME="server-status.localhost"

NAMESPACE="php-fpm"
TIMESTAMP=$(date -u '+%Y-%m-%dT%H:%M:%SZ')
HOSTNAME=$(hostname)
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:-us-east-1}
APP_NAME=${APP_NAME:-unknown}



# GET /_/php-fpm-status
STATS=$(get.sh "/_/php-fpm-status?json")

metric() {
  METRIC_NAME=$1
  VALUE=$2
  cat << EOF
{
"MetricName": "$METRIC_NAME",
"Dimensions": [ 
    {
        "Name": "AppName",
        "Value": "$APP_NAME"
    },
    {
        "Name": "ContainerHostname",
        "Value": "$HOSTNAME"
    }
],
"Timestamp": "$TIMESTAMP",
"Value": $VALUE,
"Unit": "Count"
}$3
EOF
}

FILE=$(mktemp -u)
echo "[" > $FILE
metric "IdleProcesses" $(echo $STATS | jq -r '."idle processes"') , >> $FILE
metric "ActiveProcesses" $(echo $STATS | jq -r '."active processes" -1') , >> $FILE
metric "TotalProcesses" $(echo $STATS | jq -r '."total processes"') , >> $FILE
metric "MaxActiveProcesses" $(echo $STATS | jq -r '."max active processes"') , >> $FILE
metric "MaxChildrenReached" $(echo $STATS | jq -r '."max children reached"') , >> $FILE
metric "SlowRequests" $(echo $STATS | jq -r '."slow requests"')   >> $FILE
echo "]" >> $FILE

# put-metric-data
aws cloudwatch put-metric-data \
  --region ${AWS_DEFAULT_REGION} \
  --namespace ${NAMESPACE} \
  --metric-data file://$FILE

rm $FILE
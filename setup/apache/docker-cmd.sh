#!/bin/sh
set -e

# start cache clean: every 60 minutes, max 1000 i-nodex, max 1G disk space
htcacheclean -p /var/cache/apache/ -d 60 -t -L1000 -l1G

# Apache gets grumpy about PID files pre-existing
rm -f /usr/local/apache2/logs/httpd.pid

exec httpd -DFOREGROUND
#!/usr/bin/env sh
set -e

export DOLLAR='$'
export BACKEND_SRV=${BACKEND_SRV:-missing_backend_srv}
export BACKEND_SRV_NUM=${BACKEND_SRV_NUM:-5}
export BACKEND_BALANCE=${BACKEND_BALANCE:-roundrobin}
export BACKEND_STATUS=${BACKEND_STATUS:-/status}
export BACKEND_TTL=${BACKEND_TTL:-10s}
export CACHE_SIZE=${CACHE_SIZE:-1g}
export MAX_STALE_TIME=${MAX_STALE_TIME:-10}
export TIMEOUT_IDLE=${TIMEOUT_IDLE:-65s}
export TIMEOUT_CLIENT=${TIMEOUT_CLIENT:-65s}
export TIMEOUT_SERVER=${TIMEOUT_SERVER:-65s}
export MAX_RETRIES=${MAX_RETRIES:-4}
export DNS_RESOLVER=${DNS_RESOLVER:-127.0.0.11:53}

if [ "${1#-}" != "$1" ]; then
  set -- "nuster" "$@"
fi

if [ "$1" = 'nuster' ]; then
  shift
  set -- nuster -W -db "$@"
fi

exec "$@"
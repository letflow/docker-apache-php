#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

NAME=$(basename $DIR)

TYPE=${1:-apache-php}
VERSION=${2:-3.11}
STAGE=${3:-base}
BASE=${4:-alpine}
BASE_VERSION=${5:-3.16}
IMAGE=${6:-"$NAME-$TYPE"}

##############
### Colors ###
##############
ecolor='\e[0m'
# Normal #
blue='\e[0;34m'
green='\e[0;32m'
red='\e[0;31m'
yellow='\e[0;33m'
# Bright #
bblue='\e[1;34m'
bgreen='\e[1;32m'
bred='\e[1;31m'
byellow='\e[1;33m'
##################
### End Colors ###
##################

case "$STAGE" in
 base) VARIANT="" ;;
 *) VARIANT="${STAGE}-" ;;
esac

TAG="${VARIANT}${VERSION}-$BASE${BASE_VERSION}"

DOCKER_BUILD_IMAGE="$IMAGE:$TAG"

echo -e "Building tag ${yellow}$TAG${ecolor} and image ${yellow}$DOCKER_BUILD_IMAGE${ecolor}"
docker build --tag $DOCKER_BUILD_IMAGE --file images/$TYPE/Dockerfile --build-arg VERSION=$VERSION --build-arg BASE=$BASE --build-arg BASE_VERSION=$BASE_VERSION --target=$STAGE .

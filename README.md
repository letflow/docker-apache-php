Alpine based image with PHP 7 and Apache 2.4
===

The image is developed and maintained by [LetFlow](http://www.letflow.com.ar)

GitLab: https://gitlab.com/letflow/docker-apache-php

Features:
* Based on Alpine 3.7
* Apache 2.4
* PHP 7
* Ready to be used with Laravel
* Deflate/gzip enabled
* Disk cache enabled
* Laravel rewrite enabled
* Modular apache configuration. Easy to override
* Error log to stderr. Custom log to stdout
* Optional image with supervisord configured to run laravel queues

Usage:
* Copy Laravel PHP app to /var/www/html
* Copy custom Apache conf to /etc/apache2/conf.d/
* Copy custom PHP ini to /etc/php7/conf.d/

#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

NAME=$(basename $DIR)

NGINX_VERSION=${1:-mainline}
PHP_VERSION=${2:-8.1}
STAGE=${3:-base}
BASE=${4:-alpine}
BASE_VERSION=${5:-3.19}

##############
### Colors ###
##############
ecolor='\e[0m'
# Normal #
blue='\e[0;34m'
green='\e[0;32m'
red='\e[0;31m'
yellow='\e[0;33m'
# Bright #
bblue='\e[1;34m'
bgreen='\e[1;32m'
bred='\e[1;31m'
byellow='\e[1;33m'
##################
### End Colors ###
##################

NGINX_BUILD_IMAGE="$NAME-nginx:fastcgi-${NGINX_VERSION}-${BASE}${BASE_VERSION}"
NGINX_TEST_IMAGE="nginx-test:fastcgi-${NGINX_VERSION}-${BASE}${BASE_VERSION}"

# build image if not present
if [[ "$(docker images -q $NGINX_BUILD_IMAGE 2> /dev/null)" == "" ]]; then
    echo -e "BUILD image ${yellow}$NGINX_BUILD_IMAGE${ecolor} not found. Going to build it ..."
    $DIR/build.sh nginx ${NGINX_VERSION} fastcgi ${BASE} ${BASE_VERSION}
fi

docker build --tag $NGINX_TEST_IMAGE --file $PWD/tests/nginx/Dockerfile --build-arg BASE=$NGINX_BUILD_IMAGE $PWD/tests/

PHP_BUILD_IMAGE="$NAME-php:${PHP_VERSION}-${BASE}${BASE_VERSION}"
PHP_TEST_IMAGE="php-test:${PHP_VERSION}-${BASE}${BASE_VERSION}"

# build image if not present
if [[ "$(docker images -q $PHP_BUILD_IMAGE 2> /dev/null)" == "" ]]; then
    echo -e "BUILD image ${yellow}$PHP_BUILD_IMAGE${ecolor} not found. Going to build it ..."
    $DIR/build.sh php ${PHP_VERSION} ${STAGE} ${BASE} ${BASE_VERSION}
fi

docker build --tag $PHP_TEST_IMAGE --file $PWD/tests/php/Dockerfile --build-arg BASE=$PHP_BUILD_IMAGE $PWD/tests/

PHP_CONTAINER=$(docker run --detach --rm $PHP_TEST_IMAGE)
NGINX_CONTAINER=$(docker run --detach --rm --link $PHP_CONTAINER:app --name nginx_fastcgi $NGINX_TEST_IMAGE)

docker exec $NGINX_CONTAINER /var/opt/tests/run_tests_fastcgi.sh

docker stop $NGINX_CONTAINER
docker stop $PHP_CONTAINER
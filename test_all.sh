#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

NAME=$(basename $DIR)

##############
### Colors ###
##############
ecolor='\e[0m'
# Normal #
blue='\e[0;34m'
green='\e[0;32m'
red='\e[0;31m'
yellow='\e[0;33m'
# Bright #
bblue='\e[1;34m'
bgreen='\e[1;32m'
bred='\e[1;31m'
byellow='\e[1;33m'
##################
### End Colors ###
##################

# APACHE-PHP
declare -a VERSIONS=("3.7" "3.11" "3.13")
declare -a VARIANTS=("base" "supervisord")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        echo -e "Testing ${yellow}apache-php${ecolor} Version: ${yellow}$VERSION${ecolor} Variant: ${yellow}$VARIANT${ecolor}"
        RES=$($DIR/test.sh apache-php $VERSION $VARIANT 2>/dev/null )
        if [ $? != 0 ]; then echo $RES && exit 1; fi
    done
done

# NGINX
declare -a VERSIONS=("mainline")
declare -a VARIANTS=("base" "fastcgi")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        echo -e "Testing ${yellow}nginx${ecolor} Version: ${yellow}$VERSION${ecolor} Variant: ${yellow}$VARIANT${ecolor}"
        if [ "$VARIANT" = "fastcgi" ]; then
            RES=$($DIR/test_nginx_fastcgi.sh $VERSION 2>/dev/null)
        else
            RES=$($DIR/test.sh nginx $VERSION $VARIANT 2>/dev/null)
        fi
        if [ $? != 0 ]; then echo $RES && exit 1; fi
    done
done

# NGINX-PROXY
declare -a VERSIONS=("mainline")
declare -a VARIANTS=("base")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        echo -e "Testing ${yellow}nginx${ecolor} Version: ${yellow}$VERSION${ecolor} Variant: ${yellow}$VARIANT${ecolor}"
        RES=$($DIR/test.sh nginx-proxy $VERSION $VARIANT 2>/dev/null)
        if [ $? != 0 ]; then echo $RES && exit 1; fi
    done
done

# PHP
declare -a VERSIONS=("8.0" "8.1")
declare -a VARIANTS=("base" "supervisord")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        echo -e "Testing ${yellow}php${ecolor} Version: ${yellow}$VERSION${ecolor} Variant: ${yellow}$VARIANT${ecolor}"
        RES=$($DIR/test.sh php $VERSION $VARIANT 2>/dev/null)
        if [ $? != 0 ]; then echo $RES && exit 1; fi
    done
done

# VARNISH
declare -a VERSIONS=("6.2.0")
declare -a VARIANTS=("base")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        echo -e "Testing ${yellow}varnish${ecolor} Version: ${yellow}$VERSION${ecolor} Variant: ${yellow}$VARIANT${ecolor}"
        RES=$($DIR/test.sh varnish $VERSION $VARIANT 2>/dev/null)
        if [ $? != 0 ]; then echo $RES && exit 1; fi
    done
done

# NUSTER
declare -a VERSIONS=("5.2" "5.3")
declare -a VARIANTS=("base")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        echo -e "Testing ${yellow}nuster${ecolor} Version: ${yellow}$VERSION${ecolor} Variant: ${yellow}$VARIANT${ecolor}"
        RES=$($DIR/test.sh nuster $VERSION $VARIANT 2>/dev/null)
        if [ $? != 0 ]; then echo $RES && exit 1; fi
    done
done
#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"
DOCKER_IMAGE=$1

if [ "x$DOCKER_IMAGE" = "x" ]; then
    echo "Usage: $0 <docker-image>"
    exit 1
fi

# Function to check if a command executed successfully
check_command() {
    if [ $? -ne 0 ]; then
        echo "Error executing the previous command!"
        exit 1
    fi
}

oneTimeTearDown() {
    if [ "x$CLAIR" != "x" ]; then
        echo "Stopping Clair container $CLAIR"
        docker stop $CLAIR
    fi
    
    if [ "x$DB" != "x" ]; then
        echo "Stopping DB container $DB"
        docker stop $DB
    fi
}
trap oneTimeTearDown EXIT

# Start the Clair and database containers
DB=`docker run --rm --detach -p 5432 arminc/clair-db`
check_command

echo "Starting DB container $DB ..."
DB_HOST=`docker inspect $DB  -f '{{.NetworkSettings.IPAddress}}'`
DB_PORT=5432
echo "DB starting at $DB_HOST:$DB_PORT ..."
$DIR/tests/wait-for $DB_HOST:$DB_PORT -t 60 | exit 1
echo "DB started successfully"

CLAIR=`docker run --rm -p 6060 --link $DB:postgres -d arminc/clair-local-scan`
check_command

echo "Starting Clair container $CLAIR ..."
CLAIR_HOST=`docker inspect $CLAIR  -f '{{.NetworkSettings.IPAddress}}'`
CLAIR_PORT=6060
echo "Clair starting at $CLAIR_HOST:$CLAIR_PORT ..."
$DIR/tests/wait-for $CLAIR_HOST:$CLAIR_PORT -t 60 | exit 1
echo "Clair started successfully"

# Scan the specified Docker image
echo "Analyzing $DOCKER_IMAGE"
#./bin/clair-scanner  --ip="localhost" $DOCKER_IMAGE
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock --network=container:$CLAIR ovotech/clair-scanner clair-scanner "$DOCKER_IMAGE"
check_command
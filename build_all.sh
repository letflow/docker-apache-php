#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

NAME=$(basename $DIR)

##############
### Colors ###
##############
ecolor='\e[0m'
# Normal #
blue='\e[0;34m'
green='\e[0;32m'
red='\e[0;31m'
yellow='\e[0;33m'
# Bright #
bblue='\e[1;34m'
bgreen='\e[1;32m'
bred='\e[1;31m'
byellow='\e[1;33m'
##################
### End Colors ###
##################


# APACHE-PHP
declare -a VERSIONS=("3.7" "3.8" "3.11" "3.13")
declare -a VARIANTS=("base" "supervisord")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        $DIR/build.sh apache-php $VERSION $VARIANT
    done
done

# NGINX
declare -a VERSIONS=("mainline")
declare -a VARIANTS=("base" "fastcgi")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        $DIR/build.sh nginx $VERSION $VARIANT
    done
done

# NGINX-PROY
declare -a VERSIONS=("mainline")
declare -a VARIANTS=("base")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        $DIR/build.sh nginx-proxy $VERSION $VARIANT
    done
done

# PHP
declare -a VERSIONS=("8.0" "8.1")
declare -a VARIANTS=("base" "supervisord")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        $DIR/build.sh php $VERSION $VARIANT
    done
done

# VARNISH
declare -a VERSIONS=("6.2.0")
declare -a VARIANTS=("base")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        $DIR/build.sh varnish $VERSION $VARIANT
    done
done

# NUSTER
declare -a VERSIONS=("5.2" "5.3")
declare -a VARIANTS=("base")

for VERSION in "${VERSIONS[@]}"; do
    for VARIANT in "${VARIANTS[@]}"; do
        $DIR/build.sh nuster $VERSION $VARIANT
    done
done

#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

setUp() {
    /var/opt/tests/wait-for 127.0.0.1:80
}

testStatic() {
    RESULT=`curl -s http://localhost/hello.txt`
    assertEquals "Static content" "hello world" "$RESULT"
}

testPhp() {
    RESULT=`curl -s http://localhost/php.php`
    assertEquals "PHP basic" "php working" "$RESULT"
}

testMysql() {
    RESULT=`curl -s http://localhost/mysql.php`
    assertEquals "PHP MySQL" "mysql working" "$RESULT"
}

testOpenssl() {
    RESULT=`curl -s http://localhost/openssl.php`
    assertEquals "PHP OpenSSL" "openssl working" "$RESULT"
}

testMbstring() {
    RESULT=`curl -s http://localhost/mbstring.php`
    assertEquals "PHP mbstring" "mbstring working" "$RESULT"
}

testXML() {
    RESULT=`curl -s http://localhost/xml.php`
    assertEquals "PHP xml" "xml working" "$RESULT"
}

testJSON() {
    RESULT=`curl -s http://localhost/json.php`
    assertEquals "PHP json" "json working" "$RESULT"
}

testRewrite() {
    RESULT=`curl -s http://localhost/index.php`
    assertEquals "PHP rewrite 1" "rewrite working" "$RESULT"

    RESULT=`curl -s http://localhost/rewrite/something`
    assertEquals "PHP rewrite 2" "rewrite working" "$RESULT"    
}

testSession() {
    RESULT=`curl -s http://localhost/session.php`
    assertEquals "PHP session" "session working" "$RESULT"
}

testTokenizer() {
    RESULT=`curl -s http://localhost/tokenizer.php`
    assertEquals "PHP tokenizer" "tokenizer working" "$RESULT"
}

testFileInfo() {
    RESULT=`curl -s http://localhost/fileinfo.php`
    assertEquals "PHP fileinfo" "fileinfo working" "$RESULT"
}

testGD() {
    RESULT=`curl -s http://localhost/gd.php`
    assertEquals "GD fileinfo" "GD working" "$RESULT"
}

testOpcache() {
    RESULT=`curl -s http://localhost/opcache.php`
    assertEquals "opcache fileinfo" "opcache working" "$RESULT"
}

testCurl() {
    RESULT=`curl -s http://localhost/curl.php`
    assertEquals "curl fileinfo" "curl working" "$RESULT"
}

testCache() {
    curl -I --no-progress-meter http://localhost/cache.php | grep -q "X-Cache"
    assertEquals "X-Cache present" 0 $?

    RESULT1=`curl -s http://localhost/cache.php`
    RESULT1b=`curl -s http://localhost/cache.php`
    assertEquals "cache1" "$RESULT1" "$RESULT1b"

    RESULT2=`curl -s http://localhost/cache.php?v=2`
    assertNotEquals "cache 1-2 not" "$RESULT1" "$RESULT2"
}

testCacheNone() {
    curl -I --no-progress-meter http://localhost/cachenone.php | grep -q "X-Cache: MISS"
    assertEquals "X-Cache MISS present" 0 $?

    RESULT1=`curl -s http://localhost/cachenone.php`
    RESULT1b=`curl -s http://localhost/cachenone.php`

    assertNotEquals "cache1" "$RESULT1" "$RESULT1b"
}

testError() {
    # check status 500
    STATUS=`curl -I -s http://localhost/error.php | head -n 1 |cut -d$' ' -f2`
    assertEquals "Status code" 500 $STATUS

    # not cached
    RESULT1=`curl -s http://localhost/error.php`
    RESULT1b=`curl -s http://localhost/error.php`
    assertNotEquals "cache1" "$RESULT1" "$RESULT1b"

    # valid json
    jq -e . >/dev/null 2>&1 <<< "$RESULT1";
    assertEquals "Valid JSON" 0 $?
}

testLargeError() {
    # check status 500
    STATUS=`curl -I -s http://localhost/errorlarge.php | head -n 1 |cut -d$' ' -f2`
    assertEquals "Status code" 500 $STATUS

    # not cached
    RESULT1=`curl -s http://localhost/errorlarge.php`
    RESULT1b=`curl -s http://localhost/errorlarge.php`
    assertNotEquals "cache1" "$RESULT1" "$RESULT1b"

    # valid json
    jq -e . >/dev/null 2>&1 <<< "$RESULT1";
    assertEquals "Valid JSON" 0 $?
}

testGzip() {
    curl -I --no-progress-meter http://localhost/jquery-3.3.1.min.js | grep -q "Content-Encoding: gzip"
    assertEquals "Without compression" 1 $?

    curl -I --compressed --no-progress-meter http://localhost/jquery-3.3.1.min.js | grep -q "Content-Encoding: gzip"
    assertEquals "With compression" 0 $?

    curl -I --compressed --no-progress-meter http://localhost/json.json | grep -q "Content-Encoding: gzip"
    assertEquals "JSON compression" 0 $?

    curl -s -I --compressed http://localhost/small.json | grep -q "Content-Encoding: gzip"
    assertEquals "JSON compression" 1 $?
}

testIPv6() {
    # check IPv4
    netstat -tnlp | grep tcp | grep nginx
    assertEquals "IPv4" 0 $?

    # check IPv6
    netstat -tnlp | grep tcp6 | grep nginx
    assertEquals "IPv6" 1 $?
}

# Load shUnit2.
source $DIR/shunit2
#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

PHP_TEST_IMAGE="letflow/php:8.0-alpine3.16-latest"
PHP_CONTAINER=$(docker run --detach --rm $PHP_TEST_IMAGE)

CONTAINER=$(docker run --detach --rm --tmpfs=/var/cache/nginx:uid=101 --link $PHP_CONTAINER:app $DOCKER_TEST_IMAGE)
docker exec $CONTAINER /var/opt/tests/run_tests.sh
docker stop $CONTAINER
docker stop $PHP_CONTAINER
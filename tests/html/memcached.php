<?php 

if (!extension_loaded('memcached'))
{
    die('memcached unavailable');
}

$cache = new Memcached();
$cache->setOption(Memcached::OPT_BINARY_PROTOCOL, true);
$cache->setOption(Memcached::OPT_COMPRESSION, false);
$cache->addServer('memcached', 11211);

$key = "test".rand(0, 1000000);
$value = "123 example ".rand(0, 1000000);
$cache->set($key, $value);

if ($cache->get($key) !== $value) {
    die("memcached store/retrieve failed");
}

echo "memcached working"; 
?>
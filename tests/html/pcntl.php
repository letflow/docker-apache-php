<?php 

if (!extension_loaded('pcntl'))
{
    echo 'pcntl unavailable';
    return;
}

try {
    pcntl_get_last_error();
}
catch( \Exception $e) {
    echo 'pcntl exception: ' . $e;
    return;
}

echo "pcntl working"; 
?>
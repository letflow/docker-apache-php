<?php 

$bytes = 5000;
$message = "This is my error message. \n";
$times = ceil($bytes / strlen($message));
$full = "";
foreach(range(1, $times) as $i) {
    $full .= $message;
}
error_log($full);

header("Content-Type: application/json");
header("HTTP/1.1 500 Internal Server Error");

echo json_encode([
    "error" => rand(1, 10000000),
    "bytes" => strlen($full),
    "times" => $times
]);

?>
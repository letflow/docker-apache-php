<?php 

if (!extension_loaded('imagick'))
{
    echo 'imagick unavailable';
    return;
}

if (! class_exists('Imagick')) {
    echo 'class Imagick not found';
    return;
}

echo "imagick working"; 
?>
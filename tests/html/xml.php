<?php 

if (!extension_loaded('libxml'))
{
    echo 'xml unavailable';
    return;
}


if (!extension_loaded('simplexml'))
{
    echo 'simplexml unavailable';
    return;
}

if (!class_exists('SimpleXMLElement'))
{
    echo 'SimpleXMLElement class not found';
    return;
}

if (!class_exists('XMLWriter'))
{
    echo 'XMLWriter class not found';
    return;
}

if (!class_exists('DOMDocument'))
{
    echo 'DOMDocument class not found';
    return;
}

echo "xml working"; 
?>
<?php 

if (!extension_loaded('zip'))
{
    echo 'zip unavailable';
    return;
}

try
{
    new ZipArchive;
} catch (\Exception $e) {
    echo 'zip exception' . $e;
    return;
}

echo "zip working"; 
?>
<?php 

if (!extension_loaded('gd'))
{
    echo 'GD unavailable';
    return;
}

try {
    imagecreatefromjpeg("1.jpg");
} catch( \Exception $e) {
    echo 'GD exception: ' . $e;
    return;
}

echo "GD working"; 
?>
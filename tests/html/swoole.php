<?php 

if (!extension_loaded('swoole'))
{
    echo 'Swoole unavailable';
    return;
}

try {
    swoole_version();
}
catch( \Exception $e) {
    echo 'Swoole exception: ' . $e;
    return;
}


echo "Swoole working"; 
?>
#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

setUp() {
    /var/opt/tests/wait-for 127.0.0.1:80
}

testStatic() {
    RESULT=`curl -s http://localhost/hello.txt`
    assertEquals "Static content" "hello world" "$RESULT"
}

testPhp() {
    apachectl -t -D DUMP_MODULES 2>/dev/null | grep -q php7_module
    assertEquals "PHP Module" 0 $?

    RESULT=`curl -s http://localhost/php.php`
    assertEquals "PHP basic" "php working" "$RESULT"
}

testMysql() {
    RESULT=`curl -s http://localhost/mysql.php`
    assertEquals "PHP MySQL" "mysql working" "$RESULT"
}

testOpenssl() {
    RESULT=`curl -s http://localhost/openssl.php`
    assertEquals "PHP OpenSSL" "openssl working" "$RESULT"
}

testMbstring() {
    RESULT=`curl -s http://localhost/mbstring.php`
    assertEquals "PHP mbstring" "mbstring working" "$RESULT"
}

testXML() {
    RESULT=`curl -s http://localhost/xml.php`
    assertEquals "PHP xml" "xml working" "$RESULT"
}

testJSON() {
    RESULT=`curl -s http://localhost/json.php`
    assertEquals "PHP json" "json working" "$RESULT"
}

testRewrite() {
    apachectl -t -D DUMP_MODULES 2>/dev/null | grep -q rewrite_module
    assertEquals "Rewrite Module" 0 $?

    RESULT=`curl -s http://localhost/index.php`
    assertEquals "PHP rewrite 1" "rewrite working" "$RESULT"

    RESULT=`curl -s http://localhost/rewrite/something`
    assertEquals "PHP rewrite 2" "rewrite working" "$RESULT"    
}

testSession() {
    RESULT=`curl -s http://localhost/session.php`
    assertEquals "PHP session" "session working" "$RESULT"
}

testTokenizer() {
    RESULT=`curl -s http://localhost/tokenizer.php`
    assertEquals "PHP tokenizer" "tokenizer working" "$RESULT"
}

testFileInfo() {
    RESULT=`curl -s http://localhost/fileinfo.php`
    assertEquals "PHP fileinfo" "fileinfo working" "$RESULT"
}

testGD() {
    RESULT=`curl -s http://localhost/gd.php`
    assertEquals "PHP GD" "GD working" "$RESULT"
}

testOpcache() {
    RESULT=`curl -s http://localhost/opcache.php`
    assertEquals "PHP opcache" "opcache working" "$RESULT"
}

testCurl() {
    RESULT=`curl -s http://localhost/curl.php`
    assertEquals "PHP curl" "curl working" "$RESULT"
}

testMemcached() {
    RESULT=`curl -s http://localhost/memcached.php`
    assertEquals "PHP memcached" "memcached working" "$RESULT"
}

testCache() {
    apachectl -t -D DUMP_MODULES 2>/dev/null | grep -q cache_module
    assertEquals "Cache Module" 0 $?

    apachectl -t -D DUMP_MODULES 2>/dev/null | grep -q cache_disk_module
    assertEquals "Cache Disk Module" 0 $?

    # direct request
    RESULT1=`curl -s http://localhost/cache.php`
    RESULT1b=`curl -s http://localhost/cache.php`
    assertEquals "cache1" "$RESULT1" "$RESULT1b"

    # request with redirect
    RESULT2=`curl -s http://localhost/cache2.php`
    RESULT2b=`curl -s http://localhost/cache2.php`
    assertEquals "cache2" "$RESULT2" "$RESULT2b"
    assertNotEquals "cache 1-2 not" "$RESULT1" "$RESULT2"

    # request with redirect different name
    RESULT3=`curl -s http://localhost/cache3.php`
    RESULT3b=`curl -s http://localhost/cache3.php`
    assertEquals "cache3" "$RESULT3" "$RESULT3b"
    assertNotEquals "cache 1-3 not" "$RESULT1" "$RESULT3"
    assertNotEquals "cache 2-3 not" "$RESULT2" "$RESULT3"
}

testGzip() {
    apachectl -t -D DUMP_MODULES 2>/dev/null | grep -q deflate_module
    assertEquals "Deflate Module" 0 $?

    curl -I -s http://localhost/jquery-3.3.1.min.js | grep -q "Content-Encoding: gzip"
    assertEquals "Without compression" 1 $?

    curl -I --compressed -s http://localhost/jquery-3.3.1.min.js | grep -q "Content-Encoding: gzip"
    assertEquals "With compression" 0 $?

    curl -I --compressed -s http://localhost/json.json | grep -q "Content-Encoding: gzip"
    assertEquals "JSON compression" 0 $?
}

testIPv6() {
    # check IPv4
    netstat -tnlp | grep tcp | grep httpd
    assertEquals "IPv4" 0 $?

    # check IPv6
    netstat -tnlp | grep tcp6 | grep httpd
    assertEquals "IPv6" 1 $?
}

testApacheSignature() {
    curl -I -s http://localhost/hello.txt | grep -q "Server: Apache/2.4"
    assertEquals "Apache/2.4 signature found" 1 $?
}

testPhpSignature() {
    curl -I -s http://localhost/index.php | grep -q "X-Powered-By: PHP"
    assertEquals "PHP signature found" 1 $?
}

# Load shUnit2.
source $DIR/shunit2
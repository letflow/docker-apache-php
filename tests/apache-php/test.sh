#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

MEMCACHED=$(docker run --detach --rm memcached)
CONTAINER=$(docker run --detach --rm --link $MEMCACHED:memcached --tmpfs=/var/cache/apache:uid=100 $DOCKER_TEST_IMAGE)
docker exec $CONTAINER /var/opt/tests/run_tests.sh
docker stop $CONTAINER
docker stop $MEMCACHED
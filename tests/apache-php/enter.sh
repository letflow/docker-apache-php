#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

CONTAINER=$(docker run --detach --rm --tmpfs=/var/cache/apache:uid=100 -v $PWD/tests/apache-php:/var/opt/tests $DOCKER_TEST_IMAGE)
docker exec -it $CONTAINER sh
docker stop $CONTAINER
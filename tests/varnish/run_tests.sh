#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

setUp() {
    /var/opt/tests/wait-for localhost:80
}

testStatic() {
    RESULT=`curl -s http://localhost/hello.txt`
    assertEquals "Static content" "hello world" "$RESULT"
}

testImage() {
    TMP=$(mktemp)
    curl -s http://localhost/1.jpg > $TMP
    file $TMP | grep "JPEG image data"
    assertEquals "JPEG file" 0 $?
    rm $TMP

    curl -s -I http://localhost/1.jpg | grep "Cache-Control: public, max-age=31536000, immutable"
    assertEquals "Cache-Control" 0 $?
}

testHit() {
    # ensure
    curl -I -s http://localhost/1.jpg >/dev/null
    curl -I -s http://localhost/1.jpg | grep "X-Cache: HIT"
    assertEquals "X-Cache: HIT" 0 $?
}

testStatus() {
    RESULT=`curl -s http://localhost/varnish-status | jq -r .status`
    assertEquals "Status" "ok" "$RESULT"
}

# Load shUnit2.
source $DIR/shunit2
#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

CONTAINER=$(docker run --detach --rm -p 8888:80 $DOCKER_TEST_IMAGE)
docker exec $CONTAINER nginx
docker exec -it $CONTAINER bash
docker stop $CONTAINER

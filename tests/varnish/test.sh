#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

#docker stop varnish
#docker run --rm --name varnish -p 8888:80 $DOCKER_TEST_IMAGE; exit

CONTAINER=$(docker run --detach --rm $DOCKER_TEST_IMAGE)
docker exec $CONTAINER nginx
docker exec $CONTAINER /var/opt/tests/run_tests.sh
docker stop $CONTAINER

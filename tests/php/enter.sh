#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

MEMCACHED=$(docker run --detach --rm --name php-memcached memcached)
CONTAINER=$(docker run --detach --rm --link php-memcached:memcached  $DOCKER_TEST_IMAGE)
docker logs $MEMCACHED
docker exec -it $CONTAINER sh
docker logs $MEMCACHED
docker stop $CONTAINER
docker stop $MEMCACHED
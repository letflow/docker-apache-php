#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

PHP_ROOT="/var/www/html/public"

setUp() {
    /var/opt/tests/wait-for localhost:9000
}

testPhp() {
    RESULT=$(get.sh "/php.php")
    [[ "$RESULT" == "php working" ]] || fail "PHP basic"
}

testMysql() {
    RESULT=$(get.sh "/mysql.php")
    [[ "$RESULT" == "mysql working" ]] || fail "PHP MySQL"
}

testOpenssl() {
    RESULT=$(get.sh "/openssl.php")
    [[ "$RESULT" == "openssl working" ]] || fail "PHP OpenSSL"
}

testMbstring() {
    RESULT=$(get.sh "/mbstring.php")
    [[ "$RESULT" == "mbstring working" ]] || fail "PHP mbstring"
}

testXML() {
    RESULT=$(get.sh "/xml.php")
    [[ "$RESULT" == "xml working" ]] || fail "PHP xml"
}

testJSON() {
    RESULT=$(get.sh "/json.php")
    [[ "$RESULT" == "json working" ]] || fail "PHP json"
}

testSession() {
    RESULT=$(get.sh "/session.php")
    [[ "$RESULT" == "session working" ]] || fail "PHP session"
}

testTokenizer() {
    RESULT=$(get.sh "/tokenizer.php")
    [[ "$RESULT" == "tokenizer working" ]] || fail "PHP tokenizer"
}

xtestFileInfo() {
    RESULT=$(get.sh "/fileinfo.php")
    [[ "$RESULT" == "fileinfo working" ]] || fail "PHP fileinfo"
}

testGD() {
    RESULT=$(get.sh "/gd.php")
    [[ "$RESULT" == "GD working" ]] || fail "PHP gd"
}

testImagick() {
    RESULT=$(get.sh "/imagick.php")
    [[ "$RESULT" == "imagick working" ]] || fail "PHP imagick"
}

testRedis() {
    RESULT=$(get.sh "/redis.php")
    [[ "$RESULT" == "redis working" ]] || fail "PHP redis"
}

testSwoole() {
    if [ "$(printf '%s\n' "8.0" "$PHP_VERSION" | sort -V | head -n1)" = "8.0" ]; then
        RESULT=$(get.sh "/swoole.php")
        [[ "$RESULT" == "Swoole working" ]] || fail "PHP swoole"
    else
        echo "Skip testSwoole with PHP $PHP_VERSION"
    fi
}

testPcntl() {
    RESULT=$(get.sh "/pcntl.php")
    [[ "$RESULT" == "pcntl working" ]] || fail "PHP pcntl"
}

testOpcache() {
    RESULT=$(get.sh "/opcache.php")
    [[ "$RESULT" == "opcache working" ]] || fail "PHP opcache"
}

testCurl() {
    RESULT=$(get.sh "/curl.php")
    [[ "$RESULT" == "curl working" ]] || fail "PHP curl"
}

testZip() {
    RESULT=$(get.sh "/zip.php")
    [[ "$RESULT" == "zip working" ]] || fail "PHP zip"
}

testMemcached() {
    RESULT=$(get.sh "/memcached.php")
    [[ "$RESULT" == "memcached working" ]] || fail "PHP memcached"
}

testSignature() {
    RESULT=$(get.sh "/index.php")
    echo "$RESULT" | grep -q "X-Powered-By: PHP"
    assertEquals "PHP signature found" 1 $?
}

testStatusPlain() {
    RESULT=$(get.sh "/_/php-fpm-status")
    [[ "$RESULT" == *"process manager:"* ]] || fail "PHP FPM status plain"
}

testStatusJson() {
    RESULT=$(get.sh "/_/php-fpm-status?json")
    PROCESS=$(echo $RESULT | jq -r '."process manager"')
    [[ "$PROCESS" == "dynamic" ]] || fail "PHP FPM status json"
}

# Load shUnit2.
source $DIR/shunit2
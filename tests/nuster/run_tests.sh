#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

oneTimeSetUp() {
    # wait nginx
    /var/opt/tests/wait-for localhost:8080
    # wait haproxy
    /var/opt/tests/wait-for localhost:80
    # wait backend
    for i in `seq 15` ; do
        echo "show servers state backend" | socat stdio /var/run/haproxy.sock | head -3 | tail -1 | cut -d " " -f 6 | grep 2 >/dev/null 2>&1
        
        result=$?
        if [ $result -eq 0 ] ; then
            break
        fi
        sleep 1
    done
    echo "show servers state backend" | socat stdio /var/run/haproxy.sock 
}

testBackendStatic() {
    RESULT=`curl -s http://localhost:8080/hello.txt`
    assertEquals "Static content" "hello world" "$RESULT"
}

testBackendDNS() {
    echo "BACKEND_SRV is $BACKEND_SRV"
    while read SRV; do
        SRVPARTS=($SRV)
        echo "Using backend ${SRVPARTS[3]%?}:${SRVPARTS[2]}"
        assertEquals "DNS Port ${SRVPARTS[3]%?} 8080" "8080" "${SRVPARTS[2]}"

        RESULT=`curl -s http://${SRVPARTS[3]%?}:${SRVPARTS[2]}/hello.txt`
        assertEquals "Static content from ${SRVPARTS[3]%?}" "hello world" "$RESULT"

    done << EOF
$(dig +short -t SRV $BACKEND_SRV)
EOF
}

testStatic() {
    RESULT=`curl -s http://localhost/hello.txt`
    assertEquals "Static content" "hello world" "$RESULT"
}

testImage() {
    TMP=$(mktemp)
    curl -s http://localhost/1.jpg > $TMP
    file $TMP | grep "JPEG image data"
    assertEquals "JPEG file" 0 $?
    rm $TMP

    curl -s -I http://localhost/1.jpg | grep "cache-control: public, max-age=31536000, immutable"
    assertEquals "Cache-Control" 0 $?
}

testHit() {
    # ensure
    curl -I -s http://localhost/1.jpg >/dev/null
    curl -I -s http://localhost/1.jpg | grep "x-cache: hit"
    assertEquals "X-Cache: HIT" 0 $?
}

testStatus() {
    RESULT=`curl -s http://localhost/nuster-status | jq -r .status`
    assertEquals "Status" "ok" "$RESULT"
}

testDifferentOrigin() {
    # first miss, then hit
    curl -I -s -H "Origin: http://origin1" http://localhost/1.jpg | grep "x-cache: miss"
    assertEquals "X-Cache: Origin1 MISS" 0 $?
    curl -I -s -H "Origin: http://origin1" http://localhost/1.jpg | grep "x-cache: hit"
    assertEquals "X-Cache: Origin1 HIT" 0 $?

    # change origin
    curl -I -s -H "Origin: http://origin2" http://localhost/1.jpg | grep "x-cache: miss"
    assertEquals "X-Cache: Origin2 MISS" 0 $?
    curl -I -s -H "Origin: http://origin2" http://localhost/1.jpg | grep "x-cache: hit"
    assertEquals "X-Cache: Origin2 HIT" 0 $?

    # match cors
    curl -I -s -H "Origin: http://origin1" http://localhost/1.jpg | grep "access-control-allow-origin: http://origin1"
}

testRemoveCookieRequest() {
    # verify cookie is returned
    RESULT=`curl -s --cookie "test=value" http://localhost:8080/cookie.txt`
    assertEquals "Cookie origin present" "test=value" "$RESULT"

    # verify cookie is removed
    RESULT=`curl -s --cookie "test=value" http://localhost/cookie.txt`
    assertEquals "Cookie proxy removed" "test=" "$RESULT"
}

testRemoveCookieResponse() {
    # verify cookie is returned
    curl -s -I http://localhost:8080/cookie.txt | grep "Set-cookie"
    assertEquals "Set-cookie origin present" 0 $?

    # verify cookie is removed
    curl -s -I http://localhost/cookie.txt | grep "Set-cookie"
    assertNotEquals "Set-cookie origin removed" 0 $?
}

testRemoveAuthRequest() {
    # verify auth is returned
    RESULT=`curl -s -H "Authorization: value" http://localhost:8080/authorization.txt`
    assertEquals "Auth origin present" "auth=value" "$RESULT"

    # verify cookie is removed
    RESULT=`curl -s -H "Authorization: value" http://localhost/authorization.txt`
    assertEquals "Auth proxy removed" "auth=" "$RESULT"
}

# Load shUnit2.
source $DIR/shunit2
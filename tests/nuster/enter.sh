#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

CONTAINER=$(docker run --detach --rm --name nuster -p 8888:80 -e DNS_RESOLVER="1.1.1.1:53" $DOCKER_TEST_IMAGE)
docker exec $CONTAINER nginx
#docker logs $CONTAINER
#docker attach $CONTAINER
docker exec -it $CONTAINER bash
docker stop $CONTAINER

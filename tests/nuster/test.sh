#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

#docker stop nuster
#docker run --rm --name nuster -p 8888:80 -e DNS_RESOLVER="1.1.1.1:53" $DOCKER_TEST_IMAGE; exit

CONTAINER=$(docker run --detach --rm -e DNS_RESOLVER="1.1.1.1:53" $DOCKER_TEST_IMAGE)
docker exec $CONTAINER nginx
docker exec $CONTAINER /var/opt/tests/run_tests.sh
docker stop $CONTAINER

#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

DOCKER_TEST_IMAGE=$1

PHP_TEST_IMAGE="letflow/nginx:mainline-latest"
ORIGIN_CONTAINER=$(docker run --detach --rm $PHP_TEST_IMAGE)
docker cp $DIR/../html/hello.txt $ORIGIN_CONTAINER:/var/www/html/public

CONTAINER=$(docker run --detach --rm -v $PWD/tests/nginx:/var/opt/tests --link $ORIGIN_CONTAINER:app -e PROXY_ORIGIN_PORT=80  $DOCKER_TEST_IMAGE)
docker exec -it $CONTAINER sh
docker stop $CONTAINER
docker stop $ORIGIN_CONTAINER
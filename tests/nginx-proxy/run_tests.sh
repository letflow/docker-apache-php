#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

setUp() {
    /var/opt/tests/wait-for localhost:80
}

testStatic() {
    RESULT=`curl -s http://localhost/hello.txt`
    assertEquals "Static content" "hello world" "$RESULT"
}

testIPv6() {
    # check IPv4
    netstat -tnlp | grep tcp | grep nginx
    assertEquals "IPv4" 0 $?

    # check IPv6
    netstat -tnlp | grep tcp6 | grep nginx
    assertEquals "IPv6" 1 $?
}

testGzip() {
    curl -s -I http://localhost/jquery-3.3.1.min.js | grep -q "Content-Encoding: gzip"
    assertEquals "Without compression" 1 $?

    curl -s -I --compressed http://localhost/jquery-3.3.1.min.js | grep -q "Content-Encoding: gzip"
    assertEquals "With compression" 0 $?

    curl -s -I --compressed http://localhost/json.json | grep -q "Content-Encoding: gzip"
    assertEquals "JSON compression" 0 $?

    curl -s -I --compressed http://localhost/small.json | grep -q "Content-Encoding: gzip"
    assertEquals "JSON compression" 1 $?
}

testServerToken() {
    curl -s -I http://localhost | grep -q "Server: nginx/"
    assertEquals "Server token" 1 $?
}

# Load shUnit2.
source $DIR/shunit2
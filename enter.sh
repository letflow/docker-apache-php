#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

NAME=$(basename $DIR)

TYPE=${1:-apache-php}
VERSION=${2:-3.11}
STAGE=${3:-base}
IMAGE=${4:-"$NAME-$TYPE"}

##############
### Colors ###
##############
ecolor='\e[0m'
# Normal #
blue='\e[0;34m'
green='\e[0;32m'
red='\e[0;31m'
yellow='\e[0;33m'
# Bright #
bblue='\e[1;34m'
bgreen='\e[1;32m'
bred='\e[1;31m'
byellow='\e[1;33m'
##################
### End Colors ###
##################

case "$STAGE" in
 base) VARIANT="" ;;
 *) VARIANT="${STAGE}-" ;;
esac

TAG="${VARIANT}${VERSION}"
DOCKER_BUILD_IMAGE="$IMAGE:$TAG"
DOCKER_TEST_IMAGE="$TYPE-test:$TAG"

# build image if not present
if [[ "$(docker images -q $DOCKER_BUILD_IMAGE 2> /dev/null)" == "" ]]; then
    echo -e "BUILD image not found. Going to build it ..."
    $DIR/build.sh $@
fi

# build image if not present
docker build --tag $DOCKER_TEST_IMAGE --file $PWD/tests/$TYPE/Dockerfile --build-arg BASE=$DOCKER_BUILD_IMAGE $PWD/tests/

exec $PWD/tests/$TYPE/enter.sh $DOCKER_TEST_IMAGE